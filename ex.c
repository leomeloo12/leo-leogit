#include<locale.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){
    setlocale(LC_ALL,"");
    int n1,cont,soma;

       printf("Digite o numero que deseja saber a tabuada do 1 ao 10: \n");
       scanf("%d",&n1);

       for(cont=1;cont<=10;cont++){
            soma = n1*cont;
            printf("\n%d x %d = %d",n1,cont,soma);
       }
       return 0;
}